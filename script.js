let state = {
    products: [
        {
            name: "Samsung Tablet",
            price: 200,
            isInStock: true
        },
        {
            name: "Philip SmartTV",
            price: 20000,
            isInStock: true
        },
        {
            name: "Xiaomi Tablet",
            price: 350,
            isInStock: false
        }
    ]
}
